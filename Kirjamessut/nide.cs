﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kirjamessut
{
    class Nide
    {
        Koko koko;
        string nimike;


        public Nide(string nimike, int l, int k, int p)

        {

            this.nimike = nimike;
            koko = new Koko(l, k, p);

        }

        public override string ToString()
        {

            return nimike + " koko on " + koko.ToString();

        }

    }
}
