﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kirjamessut
{
    class Program
    {
        static void Main(string[] args)
        {
            Author author = new Author("Väinö Linna");
            Book book = new Book(author, "Tuntematon sotilas");
            //author = null; //poista ja tuhoa objekti
            Tulostaja tl = new Tulostaja();
            tl.Tulosta(author);
            //Console.WriteLine(author.getName());
            Console.WriteLine(book);

            Nide nide = new Nide("Tuntematon sotilas", 45, 40, 5);
            Console.WriteLine(nide);
        }
    }
}
