﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kirjamessut
{
    class Koko
    {
        int leveys;
        int korkeus;
        int paksuus;

        public Koko(int l, int k, int p)
        {

            leveys = l;
            korkeus = k;
            paksuus = p;

        }
        public override string ToString()
        {
            return leveys + " x " + korkeus + " x " + paksuus;
        }


    }
}
